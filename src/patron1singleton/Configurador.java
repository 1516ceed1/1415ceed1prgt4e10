/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patron1singleton;

class Configurador {

    private String url;
    private String baseDatos;
    private static Configurador miconfigurador;

    public static Configurador getConfigurador(String url, String baseDatos) {
        if (miconfigurador == null) {
            miconfigurador = new Configurador(url, baseDatos);
            System.out.println("Creado objeto. ");
        } else {
            System.out.println("Error : Solo se permite una instancia ");
        }
        return miconfigurador;
    }

    private Configurador(String url, String baseDatos) {
        this.url = url;
        this.baseDatos = baseDatos;
    }

    String getUrl() {
        return url;
    }

    String getBaseDatos() {
        return baseDatos;
    }

}
