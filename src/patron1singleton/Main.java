package patron1singleton;

public class Main {

    public static void main(String[] args) {

        // Primera instancia
        Configurador c = Configurador.getConfigurador("miurl", "mibaseDatos");
        System.out.println(c.getUrl());
        System.out.println(c.getBaseDatos());

        // Segunda instancia
        Configurador c1 = Configurador.getConfigurador("miurl1", "mibaseDatos1");
        System.out.println(c1.getUrl());
        System.out.println(c1.getBaseDatos());
    }
}
/* EJECUCION :
 Creado objeto .
 miurl
 mibaseDatos
 Error : Solo se permite una instancia
 miurl
 mibaseDatos
 */
