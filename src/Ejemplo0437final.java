/**
 * Fichero: Ejemplo0606.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Ejemplo0437final {
// Clase Base

  public final void metodo1() {
  }

  public void metodo1(int i) {
    System.out.println("Prueba");
  }
}
// Clase Derivada

class clase2 extends Ejemplo0437final {

  clase2() {
  }

  /* Error: metodo1() in clase2 cannot override metodo1() in clase1;
   * overridden method is final
   */
  // public final void metodo1() { } // Error
}
// Clase Test

class final1 {

  public static void main(String args[]) {
    new clase2().metodo1(3);
  }
}

/* EJECUCION:
 Prueba
 */
