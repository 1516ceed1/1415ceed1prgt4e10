/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0440vinculacion;

/**
 * Fichero: B.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
class B extends A {

  public B() {
  }

  public void metodo() {
    System.out.println("Clase B");
  }
}
