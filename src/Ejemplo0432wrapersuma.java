/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0432wrapersuma {

  public static void main(String args[]) {
    Integer i1;
    int i;

    i1 = Integer.valueOf("22");
    i = i1.intValue();
    //i="33"; // Error
    i = (int) 3.3;
    System.out.println(i);
  }
}
