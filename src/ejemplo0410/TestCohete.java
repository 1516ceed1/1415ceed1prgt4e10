/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0410;

/**
 * Fichero: TestCohete.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
public class TestCohete {

  public static void main(String[] args) {
    Cohete c1 = new Cohete();
    Cohete c2 = new Cohete();
    System.out.println(c1.getcohetes());
    System.out.println(c2.getcohetes());
    c2.lanza();
    System.out.println(c2.getcohetes());
    // System.out.println(c2.numcohetes); Error
  }

}
/* EJECUCION:
 2
 2
 3
 */
