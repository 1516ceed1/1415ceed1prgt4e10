/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0444PersonaCasting;

/**
 * Fichero: Encargado.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Encargado extends Empleado {

  public int getSueldo() {
    Double d = new Double(sueldoBase * 1.1);
    return d.intValue();
  }
}
