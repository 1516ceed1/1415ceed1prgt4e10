/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0409;

/**
 *
 * Fichero: EjemploToString.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-nov-2015
 */
public class EjemploToString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        MiClase miclase = new MiClase();
        System.out.println(miclase);

    }

    private static class MiClase {

        String a;

        public MiClase() {
            a = "Paco";
        }

        public String toString() {
            return "Ceed " + this.a;
        }
    }

}
