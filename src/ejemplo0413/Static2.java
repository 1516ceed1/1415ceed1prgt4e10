/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0413;

/**
 * Fichero: Static2.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Static2 {

  public int dato; // variable no static

  public static void metodo() {
    // dato++; // Error
  }

  public static void main(String[] args) {
    metodo();
    // System.out.println(dato); // Error
  }
}
