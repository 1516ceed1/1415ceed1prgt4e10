
public class EnumTest {

    public enum Day {

        SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
    }
    Day day;

    public EnumTest(Day day) {
        this.day = day;
    }

    public void tellItLikeItIs() {
        switch (day) {
            case MONDAY: // Error poner : Day . MONDAY
                System.out.println(" Mondays are bad .");
                break;
            case FRIDAY:
                System.out.println(" Fridays are better .");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println(" Weekends are best .");
                break;
            default:
                System.out.println(" Midweek days are so - so .");
                break;
        }
    }

    public static void main(String[] args) {
        EnumTest firstDay = new EnumTest(Day.MONDAY);
        firstDay.tellItLikeItIs();
        EnumTest thirdDay = new EnumTest(Day.WEDNESDAY);
        thirdDay.tellItLikeItIs();
        EnumTest fifthDay = new EnumTest(Day.FRIDAY);
        fifthDay.tellItLikeItIs();
        EnumTest sixthDay = new EnumTest(Day.SATURDAY);
        sixthDay.tellItLikeItIs();
        EnumTest seventhDay = new EnumTest(Day.SUNDAY);
        seventhDay.tellItLikeItIs();
        System.out.println(" Day . MONDAY : " + Day.MONDAY);
        System.out.println(" Day . MONDAY : " + firstDay.day);
    }
}
/* EJECUCION :
 Mondays are bad .
 Midweek days are so - so .
 Fridays are better .
 Weekends are best .
 Weekends are best .
 Day . MONDAY : MONDAY
 Day . MONDAY : MONDAY
 */
