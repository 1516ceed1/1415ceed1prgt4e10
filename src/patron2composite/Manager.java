package patron2composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Manager extends Empleado {

    private String nombre;
    private double salario;
    List< Empleado> empleados = new ArrayList< Empleado>();

    public Manager(String nombre, double salario) {
        this.nombre = nombre;
        this.salario = salario;
    }

    public void add(Empleado empleado) {
        empleados.add(empleado);
    }

    public Empleado getChild(int i) {
        return empleados.get(i);
    }

    public void print() {
// System . out . println ("");
        System.out.println(" Nombre = " + this.nombre);
        System.out.println(" Salario = " + this.salario);
        //System.out.println("");
        System.out.println(" Empleados a su cargo :");
        Iterator< Empleado> empleadosIterator = empleados.iterator();
        while (empleadosIterator.hasNext()) {
            Empleado empleado = empleadosIterator.next();
            empleado.print();
        }

    }
}
