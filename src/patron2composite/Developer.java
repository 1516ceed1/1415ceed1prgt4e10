package patron2composite;

class Developer extends Empleado {
// NO SOBREESCRIBIREMOS LOS METODOS DE add , getChild y remove
// porque es un elemento HOJA

    private String nombre;
    private double salario;

    public Developer(String nombre, double salario) {
        this.nombre = nombre;
        this.salario = salario;
    }

    public void print() {
        System.out.println(" Nombre = " + this.nombre);
        System.out.println(" Salario = " + this.salario);
        //System.out.println("");
    }
}
