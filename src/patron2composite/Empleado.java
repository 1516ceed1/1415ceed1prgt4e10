package patron2composite;

public abstract class Empleado {

    public void add(Empleado empleado) {
    }

    public Empleado getChild(int i) {
        return null;
    }

    public void remove(Empleado empleado) {
    }

    public abstract void print();
}
