/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0407;

/**
 * Fichero: ThisTest.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
public class ThisTest {

  private int a;
  private String s;

  ThisTest(int i) {
    this("Texto");
    System.out.println(i);
  }

  ThisTest(String p) {
    System.out.println(p);
    this.s = p;
  }

  public static void main(String[] args) {
    ThisTest t = new ThisTest(1);
  }

}
/* Ejecucion:
 Texto
 1
 */
