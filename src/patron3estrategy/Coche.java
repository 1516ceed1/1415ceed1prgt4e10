package patron3estrategy;

class Coche {

    Strategy c;

    public void setEstrategia(Strategy c) {
        this.c = c;
    }

    public void frenar() {
        c.comofrena();
    }
}
