package patron3estrategy;

class EstrategiaSinABS implements Strategy {

    @Override
    public void comofrena() {
        System.out.println("Realizamos un frenado en 10 metros sin ABS");
    }
}
