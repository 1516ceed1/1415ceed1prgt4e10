package patron3estrategy;

class EstrategiaABS implements Strategy {

    @Override
    public void comofrena() {
        System.out.println("Realizamos un frenado en 5 metros con ABS ");
    }
}
