package patron3estrategy;

public class Main {

    public static void main(String[] args) {

        Coche context = new Coche();
        // Usamos la estrategia A
        Strategy estrategiaInicial = new EstrategiaABS();
        context.setEstrategia(estrategiaInicial);
        context.frenar();
        // Decidimos usar la estrategia B
        Strategy estrategia2 = new EstrategiaSinABS();
        context.setEstrategia(estrategia2);
        context.frenar();
        // Finalmente , usamos de nuevo la estrategia A
        context.setEstrategia(estrategiaInicial);
        context.frenar();
    }
}
/* EJECUCION :
 Realizamos un frenado en 5 metros con ABS
 Realizamos un frenado en 10 metros sin ABS
 Realizamos un frenado en 5 metros con ABS
 */
