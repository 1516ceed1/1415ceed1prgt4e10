/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0420;

/**
 * Fichero: ValorOfReferencia.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class ValorOfReferencia {

  private String param1 = new String();

  /**
   * Creates a new instance of PassValueOrReference
   */
  public ValorOfReferencia(String param1) {
    this.setParam1(param1);
  }

  public static void cambiarObjeto(ValorOfReferencia objeto) {
    objeto = new ValorOfReferencia("Este es un nuevo objeto.");
    System.out.println("Luego de \"reasignar\" pass: " + objeto);
  }

  public static void cambiarParam1(ValorOfReferencia objeto) {
    objeto.setParam1("Este es un nuevo valor para param1.");
  }

  public String getParam1() {
    return param1;
  }

  public void setParam1(String param1) {
    this.param1 = param1;
  }

  public String toString() {
    return "[param1 = " + this.getParam1() + "]";
  }

  public static void main(String[] args) {
    ValorOfReferencia pass = new ValorOfReferencia("Objeto inicial.");
    System.out.println("Entender que Java pasa parametros por valor: ");
    System.out.println("Antes de modificar pass es: " + pass);
    ValorOfReferencia.cambiarObjeto(pass);
    System.out.println("De vuelta en main pass es: " + pass);
    System.out.println("Ahora vamos a cambiar solo param1:");
    ValorOfReferencia.cambiarParam1(pass);
    System.out.println("De seguro param 1 ha cambiado: " + pass);
    System.out.println("Parece dificil, pero no lo es.");
  }
}
/* EJECUCION:
 Entender que Java pasa parametros por valor: 
 Antes de modificar pass es: [param1 = Objeto inicial.]
 Luego de "reasignar" pass: [param1 = Este es un nuevo objeto.]
 De vuelta en main pass es: [param1 = Objeto inicial.]
 Ahora vamos a cambiar solo param1:
 De seguro param 1 ha cambiado: [param1 = Este es un nuevo valor para param1.]
 Parece dificil, pero no lo es.
 */
