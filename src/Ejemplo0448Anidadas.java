/**
 * Fichero: Ejemplo0617.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejemplo0448Anidadas {

  public class Interna {

    private int x;

    void setX(int a) {
      x = a;
    }

    int getX() {
      return x;
    }
  }

  public static void main(String args[]) {
    Ejemplo0448Anidadas ex = new Ejemplo0448Anidadas();
    // Intancia de la clase interna
    Ejemplo0448Anidadas.Interna in = ex.new Interna();
    in.setX(1);
    System.out.println(in.getX());
    // Interna in1 = Interna(); Error
  }
}

/* EJECUCION
 1
 */
