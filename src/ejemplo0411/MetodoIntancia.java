/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0411;

/**
 * Fichero: MetodoIntancia.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
public class MetodoIntancia {

    public static int var = 0;
    public int var2 = 0;

    public void prueba() { // Metodo Instancia
        var = var + 2;  // miembro de la clase
        var2 = var2 + 2;  // miembro instancia
        System.out.println(var + " " + var2);
    }

    public static void main(String args[]) {
        MetodoIntancia t = new MetodoIntancia();
        t.prueba();
        MetodoIntancia t1 = new MetodoIntancia();
        t1.prueba();

    }
}
