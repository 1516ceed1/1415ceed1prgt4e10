/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0430Herencia;

/**
 * Fichero: Cuadrado.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */
public class Cuadrado extends Figura {

  private int lado;

  Cuadrado(int l) {
    lado = l;
  }

  public int area() {
    return lado * lado;
  }

  public int getLado() {
    return lado;
  }

  public void mostrar() {
    //  getColor() es heredado de Figura.
    System.out.println("Cuadrado: Color: " + getColor() + " Lado: " + getLado());
  }
}
