import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Ejemplo0436AbstractIGU extends JFrame implements ActionListener {
  // Implementa el interface ActionListener
  // Hereda de la Clase JFrame

  public Ejemplo0436AbstractIGU() {
    setSize(400, 400); // Hereda de JFrame
    JButton boton = new JButton("Start");
    boton.addActionListener(this);
    add(boton);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent a) { // Error si se omite la funcion
    for (int i = 0; i < 1000; i++) { // Bucle 
      System.out.println(i);
    }
  }

  public static void main(String[] args) {
    new Ejemplo0436AbstractIGU();
  }
}
