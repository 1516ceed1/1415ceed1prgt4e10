/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personastatic;

/**
 *
 * Fichero: Persona.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-nov-2015
 */
class Persona {

    private String id;
    private static int idsig = 1;
    private String nombre;

    public Persona() {
        id = " " + idsig;
        idsig++;

    }

    /**
     * @return the idsig
     */
    public static int getIdsig() {
        return idsig;
    }

    /**
     * @param aIdsig the idsig to set
     */
    public static void setIdsig(int aIdsig) {
        idsig = aIdsig;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
