/*
 Hacer un programa java en el paquete personastatic que tenga:
 - La clase Persona con atributos id y nombre.
 - La clase Main creará objetos hasta que el nombre sea un fin. El atributo id se calculará de forma automática a partir del numero inicial 1.

 linea = "paco"
 linea.equals("paco") --> true
 */
package personastatic;

import java.util.Scanner;

/**
 *
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-nov-2015
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String linea;
        Scanner sc = new Scanner(System.in);
        do {

            Persona persona = new Persona();
            System.out.println("Persona");
            System.out.println("Id: " + persona.getId());
            System.out.print("Nombre: ");
            linea = sc.nextLine();
            if (!linea.equals("fin")) {
                persona.setNombre(linea);
            }

        } while (!linea.equals("fin"));

    }

}
